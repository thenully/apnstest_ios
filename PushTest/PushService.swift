//
//  PushService.swift
//  PushTest
//
//  Created by Scott Lee on 30/10/2018.
//  Copyright © 2018 Thenully Inc. All rights reserved.
//

import Foundation
import RxSwift

class PushService {
    
    static let instance:PushService = PushService()
    
    private init(){}
    
    public var tokenObserver:ReplaySubject<String> = ReplaySubject<String>.create(bufferSize: 1)
    
    public var messageObserver:ReplaySubject<String> = ReplaySubject<String>.create(bufferSize: 10)
    
}
