//
//  FirstViewController.swift
//  PushTest
//
//  Created by Scott Lee on 30/10/2018.
//  Copyright © 2018 Thenully Inc. All rights reserved.
//

import UIKit
import RxSwift

class FirstViewController: UIViewController {

    @IBOutlet weak var pushField: UITextField!
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PushService.instance.tokenObserver.observeOn(MainScheduler.asyncInstance).subscribe({ [weak self] (message) in
            self?.pushField.text = message.element ?? "N/A"
        }).disposed(by: disposeBag)
        
    }

}

