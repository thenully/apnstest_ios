//
//  SecondViewController.swift
//  PushTest
//
//  Created by Scott Lee on 30/10/2018.
//  Copyright © 2018 Thenully Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class SecondViewController: UIViewController {

    
    @IBOutlet weak var textView: UITextView!
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PushService.instance.messageObserver
            .observeOn(MainScheduler.asyncInstance)
            .subscribe { (message) in
            self.textView.insertText(message.element ?? "\n")
        }.disposed(by: disposeBag)

    }


}

